from config.database import Base
from sqlalchemy import Column, Integer, String, Float

#-----------------------------------------------------------------------------------------------------------------------------------------#

class Chompu(Base):

    __tablename__="chompu"

    id = Column(Integer, primary_key = True)
    marca = Column(String)
    ram = Column(Integer)
    almacenamiento = Column(Integer)
    modelo = Column(String)
    color = Column(String) 

#-----------------------------------------------------------------------------------------------------------------------------------------#