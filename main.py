from fastapi import FastAPI
from fastapi.responses import HTMLResponse
from config.database import engine, Base
from middlewares.error_handler import ErrorHandler
from routers.movie import movie_router
from routers.user import user_router
from routers.chompu import chompu_router

#-----------------------------------------------------------------------------------------------------------------------------------------#

app = FastAPI()
app.title = "fast api god"
app.version = "0.0.1"

#-----------------------------------------------------------------------------------------------------------------------------------------#

app.add_middleware(ErrorHandler)
app.include_router(user_router)
app.include_router(movie_router)
app.include_router(chompu_router)
Base.metadata.create_all(bind=engine)

#-----------------------------------------------------------------------------------------------------------------------------------------#

@app.get('/', tags=['home'])
def message():
    return HTMLResponse('<h1>Bienvenido a la Página de Maximo :D</h1>')

#-----------------------------------------------------------------------------------------------------------------------------------------#
