from models.chompu import Chompu as ChompuModel
from schemas.chompu import Chompu
from fastapi.encoders import jsonable_encoder

#-----------------------------------------------------------------------------------------------------------------------------------------#

class ChompuService():
    
    def __init__(self, db) -> None:
        self.db=db

#-----------------------------------------------------------------------------------------------------------------------------------------#

    def get_chompus(self):
        result = self.db.query(ChompuModel).all()
        if not result:
            return {"message": "No hay chompus"}, 404
        return jsonable_encoder(result), 200

#-----------------------------------------------------------------------------------------------------------------------------------------#

    def get_chompu(self, id: int):
        result = self.db.query(ChompuModel).filter(ChompuModel.id == id).first()
        if not result:
            return {"message": "No se encontro"}, 404
        return jsonable_encoder(result), 200

#-----------------------------------------------------------------------------------------------------------------------------------------#

    def get_chompus_by_marca(self, marca: str):
        result = self.db.query(ChompuModel).filter(ChompuModel.marca == marca).all()
        if not result:
            return {"message": "No se encontro"}, 404
        return jsonable_encoder(result), 200

#-----------------------------------------------------------------------------------------------------------------------------------------#

    def create_chompu(self, chompu: Chompu):
        new_chompu = ChompuModel(**chompu.model_dump())
        self.db.add(new_chompu)
        self.db.commit()
        return {"message": "Chompu creada"}, 200

#-----------------------------------------------------------------------------------------------------------------------------------------#

    def update_chompu(self, id: int, data: Chompu):
        result = self.db.query(ChompuModel).filter(ChompuModel.id == id).first()
        if not result:
            return {"message": "No se encontro"}, 404
        result.color = data.color
        result.modelo = data.modelo
        result.ram = data.ram
        result.almacenamiento = data.almacenamiento
        result.marca = data.marca
        self.db.commit()
        return{"message": "Se actualizo la chompu"}, 200

#-----------------------------------------------------------------------------------------------------------------------------------------#

    def delete_chompu(self, id: int):
        result = self.db.query(ChompuModel).filter(ChompuModel.id == id).first()
        if not result:
            return {"message": "No se encontro"}, 404
        self.db.delete(result)
        self.db.commit()
        return jsonable_encoder("message", "Chompu borrada"), 200
    
#-----------------------------------------------------------------------------------------------------------------------------------------#
