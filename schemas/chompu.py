from pydantic import BaseModel, Field
from typing import Optional

#-----------------------------------------------------------------------------------------------------------------------------------------#

class Chompu(BaseModel):
    id: Optional[int] = None
    marca: str = Field(min_length=1, max_length=100) 
    ram: int = Field(le=1000)
    almacenamiento: int = Field(ge=1, le=10000)
    modelo: str = Field(min_length=1, max_length=50)
    color: str = Field(min_length=1, max_length=15)

#-----------------------------------------------------------------------------------------------------------------------------------------#

    class Config:
        schema_extra = { 
            "examples": [{  
                "id": 1,
                "marca": "Hp",
                "ram": 32,
                "almacenamiento": 128,
                "modelo": "Inspiron",
                "color": "rojo"
            }]
        }

#-----------------------------------------------------------------------------------------------------------------------------------------#