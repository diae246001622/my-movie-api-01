from fastapi import Path, Query, Depends
from fastapi.responses import JSONResponse
from pydantic import BaseModel, Field
from typing import Optional, List
from config.database import Session
from models.chompu import Chompu as ChompuModel
from fastapi.encoders import jsonable_encoder
from middlewares.jwt_bearer import JWTBaerer
from fastapi import APIRouter
from services.chompu import ChompuService
from schemas.chompu import Chompu

#------------------, dependencies=[Depends(JWTBaerer())]-----------------------------------------------------------------------------------------------------#

chompu_router = APIRouter()

#-----------------------------------------------------------------------------------------------------------------------------------------#

@chompu_router.get('/chompus', tags=['chompus'], response_model=List[Chompu], status_code=200) 
def get_chompus() -> List[Chompu]:
    db = Session()
    result = ChompuService(db).get_chompus()
    return JSONResponse(status_code=200, content=jsonable_encoder(result))

#-----------------------------------------------------------------------------------------------------------------------------------------#

@chompu_router.get('/chompus/{id}', tags=['chompus'], response_model=Chompu)
def get_chompu(id: int = Path(ge=1, le=2000)) -> Chompu:
    db = Session()
    result = ChompuService(db).get_chompu(id)
    if not result:
        return JSONResponse(status_code=404, content={'message': "No encontrado"})
    return JSONResponse(status_code=200, content=jsonable_encoder(result))

#-----------------------------------------------------------------------------------------------------------------------------------------#

@chompu_router.get('/chompus/', tags=['chompus'], response_model=List[Chompu]) 
def get_chompus_by_marca(marca: str = Query(min_length=1, max_length=15)):
    db = Session()
    result = ChompuService(db).get_chompus_by_marca(marca)
    return JSONResponse(status_code=200, content=jsonable_encoder(result))

#-----------------------------------------------------------------------------------------------------------------------------------------#

@chompu_router.post('/chompus', tags=['chompus'], response_model=dict, status_code=200)
def create_chompu(chompu : Chompu) -> dict:
    db = Session()
    ChompuService(db).create_chompu(chompu)
    return JSONResponse(status_code=200, content={"message": "Se ha registrado la chompu"})

#-----------------------------------------------------------------------------------------------------------------------------------------#

@chompu_router.delete('/chompus/{id}', tags=['chompus'])
def delete_chompu(id: int):
    db = Session()
    ChompuService(db).delete_chompu(id)
    return JSONResponse(status_code=200, content={"message": "Se ha Eliminado la chompu"})

#-----------------------------------------------------------------------------------------------------------------------------------------#

@chompu_router.put('/chompus/{id}', tags=['chompus'], response_model=dict, status_code=200)
def update_chompu(id: int, chompu: Chompu) -> dict:
    db = Session()
    ChompuService(db).update_chompu(id, chompu)
    return JSONResponse(status_code=200,content={"message": "Se ha modificado la chompu"})       

#-----------------------------------------------------------------------------------------------------------------------------------------#
