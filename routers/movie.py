from fastapi import Path, Query, Depends
from fastapi.responses import JSONResponse
from pydantic import BaseModel, Field
from typing import Optional, List
from config.database import Session
from models.movie import Movie as MovieModel
from fastapi.encoders import jsonable_encoder
from middlewares.jwt_bearer import JWTBaerer
from fastapi import APIRouter
from services.movie import MovieService
from schemas.movie import Movie

#------------------, dependencies=[Depends(JWTBaerer())]-----------------------------------------------------------------------------------------------------#

movie_router = APIRouter()

#-----------------------------------------------------------------------------------------------------------------------------------------#

@movie_router.get('/movies', tags=['movies'], response_model=List[Movie], status_code=200) 
def get_movies() -> List[Movie]:
    db = Session()
    result = MovieService(db).get_movies()
    return JSONResponse(status_code=200, content=jsonable_encoder(result))

#-----------------------------------------------------------------------------------------------------------------------------------------#

@movie_router.get('/movies/{id}', tags=['movies'], response_model=Movie)
def get_movie(id: int = Path(ge=1, le=2000)) -> Movie:
    db = Session()
    result = MovieService(db).get_movie(id)
    if not result:
        return JSONResponse(status_code=404, content={'message': "No encontrado"})
    return JSONResponse(status_code=200, content=jsonable_encoder(result))

#-----------------------------------------------------------------------------------------------------------------------------------------#

@movie_router.get('/movies/', tags=['movies'], response_model=List[Movie]) 
def get_movies_by_category(category: str = Query(min_length=1, max_length=15)):
    db = Session()
    result = MovieService(db).get_movies_by_category(category)
    return JSONResponse(status_code=200, content=jsonable_encoder(result))

#-----------------------------------------------------------------------------------------------------------------------------------------#

@movie_router.post('/movies', tags=['movies'], response_model=dict, status_code=200)
def create_movie(movie : Movie) -> dict:
    db = Session()
    MovieService(db).create_movie(movie)
    return JSONResponse(status_code=200, content={"message": "Se ha registrado la pelicula"})

#-----------------------------------------------------------------------------------------------------------------------------------------#

@movie_router.delete('/movies/{id}', tags=['movies'])
def delete_movie(id: int):
    db = Session()
    MovieService(db).delete_movie(id)
    return JSONResponse(status_code=200, content={"message": "Se ha Eliminado la pelicula"})

#-----------------------------------------------------------------------------------------------------------------------------------------#

@movie_router.put('/movies/{id}', tags=['movies'], response_model=dict, status_code=200)
def update_movies(id: int, movie: Movie) -> dict:
    db = Session()
    MovieService(db).update_movies(id, movie)
    return JSONResponse(status_code=202,content={"message": "Se ha modificado la pelicula"})       

#-----------------------------------------------------------------------------------------------------------------------------------------#
